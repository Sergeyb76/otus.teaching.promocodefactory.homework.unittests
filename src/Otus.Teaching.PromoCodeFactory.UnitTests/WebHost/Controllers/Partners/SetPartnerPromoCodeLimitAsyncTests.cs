﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock = new Mock<IRepository<Partner>>();
        private readonly PartnersController _partnersController;
                
        private static Guid fakePartnerId = Guid.Empty;
        private static Guid inactivePartnerId = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");
        private static Guid activePartnerId = Guid.Parse("894b6e9b-eb5f-406c-aefa-8ccb35d39319");

        private static Partner GetPartnerById(Guid id)
        {
            return FakeDataFactory.Partners.FirstOrDefault(x => x.Id == id);
        }

        private async Task<Partner> GetRepPartnerById(Guid id)
        {
            return await _partnersRepositoryMock.Object.GetByIdAsync(id);
        }

        public SetPartnerPromoCodeLimitAsyncTests()
        {            
            _partnersController = new PartnersController(_partnersRepositoryMock.Object);
        }

        // --------------------------------------------------------------------------------------------------
        // 1: Если партнер не найден, то также нужно выдать ошибку 404;
        // --------------------------------------------------------------------------------------------------

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartherNotFound_ReturnNotFound()
        {
            // arrange:
            Guid partnerId = fakePartnerId;
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest();

            Partner partner = GetPartnerById(partnerId);

            _partnersRepositoryMock.Setup(rep => rep.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var repPartner = await GetRepPartnerById(partnerId);

            // act:
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // assert:
            
            // Check: repPartner is null && result is NotFoundResult
            result.Should().BeOfType<NotFoundResult>();
            repPartner.Should().BeNull();
        }

        // --------------------------------------------------------------------------------------------------
        // 2: Если партнер заблокирован, то есть поле IsActive = false в классе Partner, то также нужно выдать ошибку 400;
        // --------------------------------------------------------------------------------------------------

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerInactive_ReturnBadRequest()
        {
            // arrange:
            Guid partnerId = inactivePartnerId;
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest();

            Partner partner = GetPartnerById(partnerId);

            _partnersRepositoryMock.Setup(rep => rep.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var repPartner = await GetRepPartnerById(partnerId);

            // act:
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // assert:

            // Check: repPartner.IsActive = false && result is BadRequestObjectResult
            result.Should().BeOfType<BadRequestObjectResult>();
            repPartner.IsActive.Should().BeFalse();
        }

        // --------------------------------------------------------------------------------------------------
        // 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал (NumberIssuedPromoCodes); если лимит закончился, то количество не обнуляется;
        // --------------------------------------------------------------------------------------------------

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_ClearNumberIssuedPromoCodes()
        {
            // arrange:
            Guid partnerId = activePartnerId;
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now.AddMonths(1),
                Limit = 222
            };

            Partner partner = GetPartnerById(partnerId);
            partner.NumberIssuedPromoCodes = 123;

            _partnersRepositoryMock.Setup(rep => rep.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var repPartner = await GetRepPartnerById(partnerId);

            // act:
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // assert:

            // Check: repPartner.NumberIssuedPromoCodes is 0 && result is CreatedAtActionResult
            result.Should().BeOfType<CreatedAtActionResult>();
            repPartner.NumberIssuedPromoCodes.Should().Be(0);
        }

        // --------------------------------------------------------------------------------------------------
        // 4. При установке лимита нужно отключить предыдущий лимит
        // --------------------------------------------------------------------------------------------------

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_CloseActivePartnerLimit()
        {
            // arrange:
            Guid partnerId = activePartnerId;
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now.AddMonths(1),
                Limit = 333
            };

            Partner partner = GetPartnerById(partnerId);

            _partnersRepositoryMock.Setup(rep => rep.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // act:
            var activeLimit = (await GetRepPartnerById(partnerId)).PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // assert:

            // Check: activeLimit.CancelDate.HasValue && result is CreatedAtActionResult
            result.Should().BeOfType<CreatedAtActionResult>();
            activeLimit.CancelDate.HasValue.Should().BeTrue();
        }

        // --------------------------------------------------------------------------------------------------
        // 5. Лимит должен быть больше 0;
        // --------------------------------------------------------------------------------------------------

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitLessThanOne_ReturnBadRequest()
        {
            // arrange:
            Guid partnerId = activePartnerId;
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now.AddMonths(1),
                Limit = 0
            };

            Partner partner = GetPartnerById(partnerId);

            _partnersRepositoryMock.Setup(rep => rep.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // act:
            var oldLimitCnt = (await GetRepPartnerById(partnerId)).PartnerLimits.Count();

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            var newLimitCnt = (await GetRepPartnerById(partnerId)).PartnerLimits.Count();

            // assert:

            // Check: request.Limit < 1 && oldLimitCnt == newLimitCnt && result is BadRequestObjectResult
            result.Should().BeOfType<BadRequestObjectResult>();
            request.Limit.Should().BeLessThan(1);
            oldLimitCnt.Should().Equals(newLimitCnt);
        }

        // --------------------------------------------------------------------------------------------------
        // 6. Нужно убедиться, что сохранили новый лимит в базу данных(это нужно проверить Unit-тестом);
        // --------------------------------------------------------------------------------------------------

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_AddLimitToDB()
        {
            // arrange:
            Guid partnerId = activePartnerId;
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now.AddMonths(1), 
                Limit = 444
            };

            Partner partner = GetPartnerById(partnerId);

            _partnersRepositoryMock.Setup(rep => rep.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // act:
            var oldLimitCnt = (await GetRepPartnerById(partnerId)).PartnerLimits.Count();

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            var newLimitCnt = (await GetRepPartnerById(partnerId)).PartnerLimits.Count();

            // assert:

            // Check: oldLimitCnt < newLimitCnt) && result is CreatedAtActionResult
            result.Should().BeOfType<CreatedAtActionResult>();
            oldLimitCnt.Should().BeLessThan(newLimitCnt);
        }
    }
}